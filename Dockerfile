From ubuntu:latest

RUN apt-get -y update && \
  apt-get -y full-upgrade && \
  apt-get -y install net-tools curl tcpdump lsof iperf netcat sudo iputils-ping tcptraceroute nmap httping wget python-pip && \
  pip install --upgrade pip && \
  pip install supervisor